#include "AudioManager.hpp"

sf::Music AudioManager::music;
sf::SoundBuffer AudioManager::buffer;
sf::Sound AudioManager::sound;

void AudioManager::playSound(const std::string& filePath) {

	if (!buffer.loadFromFile(filePath)) {
		return;
	}

	
	sound.setBuffer(buffer);
	sound.play();
}

void AudioManager::playMusic(const std::string& filePath, bool loop) {
	if (!music.openFromFile(filePath)) {
		return;
	}

	music.setVolume(30.0f);
	music.setLoop(loop);
	music.play();
}
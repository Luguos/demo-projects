#include <string>
#include <SFML/Audio.hpp>

class AudioManager {
private:
	static sf::SoundBuffer buffer;
	static sf::Sound sound;
	static sf::Music music;
public:
	static void playSound(const std::string& filePath);
	static void playMusic(const std::string& filePath, bool loop);
};
#include "GameManager.hpp"
#include "SpriteObjects/Bar.hpp"
#include "SpriteObjects/Ball.hpp"
#include "SpriteObjects/Brick.hpp"
#include "Text/TextManager.hpp"
#include "Audio/AudioManager.hpp"

int GameManager::score = 0;
sf::Text* GameManager::scoreText;
bool GameManager::isFinished = false;
bool GameManager::isGameOver = false;

void GameManager::create(std::vector<GameObject*>& gameObjects, sf::RenderTarget& window)
{
	createGameObjects(gameObjects);
	createText();
	startAudio();
}

void GameManager::createGameObjects(std::vector<GameObject*>& gameObjects)
{
	Ball* ball = new Ball("Resources/Textures/ball.png", sf::Vector2f(0, 0), 500.0f);
	gameObjects.push_back(ball);

	Bar* bar = new Bar("Resources/Textures/bar.png", sf::Vector2f(64, 672), 1000.0f, ball);
	gameObjects.push_back(bar);

	sf::Vector2f brickAreaSize = sf::Vector2f(7, 5);
	sf::Vector2f brickAreaOffset = sf::Vector2f(64, 64);
	createBricks("Resources/Textures/brick.png", brickAreaSize, brickAreaOffset, gameObjects);
}

void GameManager::createBricks(const std::string& brickTexturePath, sf::Vector2f& brickAreaSize, sf::Vector2f& brickAreaOffset, std::vector<GameObject*>& gameObjects) {
	sf::Texture brickTexture;
	if (!brickTexture.loadFromFile(brickTexturePath)) {
		return;
	}
	sf::Vector2u brickSize = brickTexture.getSize();

	for (int row = 0; row < brickAreaSize.y; row++) {
		for (int col = 0; col < brickAreaSize.x; col++) {
			int xPos = col * brickSize.x;
			int yPos = row * brickSize.y;

			sf::Color color;
			int health;
			int points;
			switch (row) {
			case 0:
				color = sf::Color(255, 55, 55);
				health = 1;
				points = 5;
				break;
			case 1:
				color = sf::Color(255, 95, 31);
				health = 1;
				points = 4;
				break;
			case 2:
				color = sf::Color(255, 255, 55);
				health = 1;
				points = 3;
				break;
			case 3:
				color = sf::Color(55, 255, 55);
				health = 1;
				points = 2;
				break;
			case 4:
				color = sf::Color(55, 55, 255);
				health = 1;
				points = 1;
				break;
			}

			Brick* brick = new Brick(brickTexturePath, sf::Vector2f((float)xPos + brickAreaOffset.x, (float)yPos + brickAreaOffset.y), color, health, points);
			gameObjects.push_back(brick);
		}
	}
}

void GameManager::startAudio() {
	AudioManager::playMusic("Resources/Audio/BGM.wav", true);
}

void GameManager::createText()
{
	TextManager::loadFont("Resources/Fonts/VT323.ttf");
	scoreText = TextManager::createText("", 32, sf::Color::White, sf::Vector2f(8, 8));
	updateScoreText();
}

void GameManager::update(std::vector<GameObject*>& gameObjects, sf::RenderTarget& window, sf::Clock& clock) {
	float elapsedTime = clock.restart().asSeconds();

	int activeBricks = 0;
	for (size_t i = 0; i < gameObjects.size(); i++) {
		GameObject* gameObject = gameObjects[i];
		if (gameObject->isActive()) {
			if (dynamic_cast<Brick*>(gameObject)) {
				activeBricks++;
			}

			if (!isGameOver && !isFinished) {
				gameObject->update(window, elapsedTime);
				checkCollisions(gameObject, gameObjects);
			}

			window.draw(*gameObject);
		}
	}

	if (activeBricks == 0) {
		enableFinished(window);
	}

	TextManager::draw(window);

	if (isGameOver || isFinished) {
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::R)) {
			restart(gameObjects, window);
		}
	}
}

void GameManager::restart(std::vector<GameObject*>& gameObjects, sf::RenderTarget& window)
{
	score = 0;
	clean(gameObjects);
	TextManager::clean();

	create(gameObjects, window);
	isFinished = false;
	isGameOver = false;
}

void GameManager::clean(std::vector<GameObject*>& gameObjects) {
	for (size_t i = 0; i < gameObjects.size(); i++) {
		delete[] gameObjects[i];
	}

	gameObjects.clear();
}

void GameManager::checkCollisions(GameObject* gameObject, std::vector<GameObject*>& gameObjects) {
	SpriteObject* currentObject = dynamic_cast<SpriteObject*>(gameObject);
	if (!currentObject) {
		return;
	}

	sf::FloatRect currentBounds = currentObject->getTransform().transformRect(currentObject->getLocalBounds());
	for (size_t i = 0; i < gameObjects.size(); i++) {
		SpriteObject* otherObject = dynamic_cast<SpriteObject*>(gameObjects[i]);
		if ((!otherObject || currentObject == otherObject)) {
			continue;
		}

		if (otherObject->isActive()) {
			sf::FloatRect otherBounds = otherObject->getTransform().transformRect(otherObject->getLocalBounds());
			sf::FloatRect intersection;
			bool hasCollision = currentBounds.intersects(otherBounds, intersection);
			if (hasCollision) {
				currentObject->onCollision(otherObject, intersection);
			}
		}
	}
}

void GameManager::enableFinished(sf::RenderTarget& window) {
	isFinished = true;
	enableCenterText("Congratulations, you finished the game! Press 'R' to restart.", window);
}

void GameManager::enableGameOver(sf::RenderTarget& window) {
	isGameOver = true;
	enableCenterText("Game over! Press 'R' to restart.", window);
}

void GameManager::enableCenterText(const std::string& text, sf::RenderTarget& window)
{
	sf::Vector2u windowSize = window.getSize();
	sf::Vector2f position = sf::Vector2f((float)windowSize.x / 2, (float)windowSize.y / 2);
	sf::Text* textObject = TextManager::createText(text, 32, sf::Color::White, position);
	sf::FloatRect textBounds = textObject->getLocalBounds();
	textObject->setPosition(position.x - (textBounds.width / 2), position.y - (textBounds.height / 2));
}

void GameManager::addPoints(int points) {
	score += points;
	updateScoreText();
}

void GameManager::updateScoreText() {
	scoreText->setString("Score: " + std::to_string(score));
}
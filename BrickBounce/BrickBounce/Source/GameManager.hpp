#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

#include <string>
#include <vector>
#include <SFML/Graphics.hpp>
#include "GameObject.hpp"

class GameManager {
private:
	static int score;
	static sf::Text* scoreText;
	static bool isFinished;
	static bool isGameOver;

	static void createGameObjects(std::vector<GameObject*>& gameObjects);
	static void createBricks(const std::string& brickTexturePath, sf::Vector2f& brickAreaSize, sf::Vector2f& brickAreaOffset, std::vector<GameObject*>& gameObjects);
	static void createText();
	static void startAudio();
	static void checkCollisions(GameObject* gameObject, std::vector<GameObject*>& gameObjects);
	static void restart(std::vector<GameObject*>& gameObjects, sf::RenderTarget& window);
	static void enableCenterText(const std::string& text, sf::RenderTarget& window);
	static void updateScoreText();
public:
	static void create(std::vector<GameObject*>& gameObjects, sf::RenderTarget& window);
	static void update(std::vector<GameObject*>& gameObjects, sf::RenderTarget& window, sf::Clock& clock);
	static void clean(std::vector<GameObject*>& gameObjects);
	static void addPoints(int points);
	static void enableFinished(sf::RenderTarget& window);
	static void enableGameOver(sf::RenderTarget& window);
};

#endif
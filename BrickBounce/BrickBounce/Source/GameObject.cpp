#include "GameObject.hpp"

GameObject::GameObject(sf::Vector2f position): sf::Drawable(), sf::Transformable() {
	active = true;
	setPosition(position);
}

void GameObject::setActive(bool active) {
	this->active = active;
}

bool GameObject::isActive() {
	return active;
}
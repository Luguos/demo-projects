#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <string>
#include <SFML/Graphics.hpp>

class GameObject : public sf::Drawable, public sf::Transformable {
private:
	bool active;
public:
	GameObject(sf::Vector2f position);
	virtual void update(sf::RenderTarget& window, float elapsedTime) = 0;
	virtual void draw(sf::RenderTarget& window, sf::RenderStates states) const override = 0;
	void setActive(bool active);
	bool isActive();
};

#endif
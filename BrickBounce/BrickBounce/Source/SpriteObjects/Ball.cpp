#include "Source/Utilities/VectorUtilities.hpp"
#include "Source/GameManager.hpp"
#include "Brick.hpp"
#include "Ball.hpp"
#include "Source/Audio/AudioManager.hpp"

Ball::Ball(const std::string& texturePath, sf::Vector2f position, float speed) : SpriteObject(position, texturePath) {
	this->speed = speed;
	this->attachedToObject = nullptr;
}

void Ball::update(sf::RenderTarget& window, float elapsedTime) {
	if (attachedToObject) {
		float ballWidth = sprite.getTexture()->getSize().x * sprite.getScale().x;
		float barWidth = (float)attachedToObject->getSprite().getTexture()->getSize().x;
		float barHeight = (float)attachedToObject->getSprite().getTexture()->getSize().y;

		setPosition(attachedToObject->getPosition() + sf::Vector2f((barWidth / 2) - (ballWidth / 2), -barHeight));
		velocity = sf::Vector2f(0, 0);
	}

	move(velocity * speed * elapsedTime);

	if (getPosition().x < 0 || getPosition().x > window.getSize().x - sprite.getTexture()->getSize().x) {
		velocity.x = -velocity.x;
		playBounceSound();
	}

	if (getPosition().y < 0) {
		velocity.y = -velocity.y;
		playBounceSound();
	}

	if (getPosition().y > window.getSize().y - sprite.getTexture()->getSize().y) {
		setActive(false);
		GameManager::enableGameOver(window);
		return;
	}

	SpriteObject::update(window, elapsedTime);
}

void Ball::draw(sf::RenderTarget& window, sf::RenderStates states) const {
	SpriteObject::draw(window, states);
}

void Ball::onCollision(SpriteObject* other, sf::FloatRect intersection) {
	if (attachedToObject) {
		return;
	}

	Brick* brick = dynamic_cast<Brick*>(other);
	if (brick) {
		brick->hit();
	}

	bounce(other, intersection);
}

void Ball::bounce(SpriteObject* other, sf::FloatRect& intersection) {
	sf::Vector2f collisionNormal = other->getPosition() - getPosition();
	sf::Vector3f collisionSolution = solveCollision(intersection, collisionNormal);

	sf::Vector2f offset = sf::Vector2f(collisionSolution.x, collisionSolution.y);
	move(offset * collisionSolution.z);
	sf::Vector2f newVelocity = reflect(velocity, offset);
	velocity = newVelocity;
	playBounceSound();
}

void Ball::playBounceSound() {
	AudioManager::playSound("Resources/Audio/Bounce.wav");
}

sf::Vector3f Ball::solveCollision(const sf::FloatRect& intersection, const sf::Vector2f& collisionNormal) {
	sf::Vector3f solution;

	if (intersection.width < intersection.height) {
		solution.x = collisionNormal.x < 0 ? 1.0f : -1.0f;
		solution.z = intersection.width;
	}
	else {
		solution.y = collisionNormal.y < 0 ? 1.0f : -1.0f;
		solution.z = intersection.height + 1.0f;
	}

	return solution;
}

void Ball::setAttach(SpriteObject* other) {
	attachedToObject = other;
}

void Ball::setVelocity(sf::Vector2f velocity) {
	this->velocity = normalize(velocity);
}
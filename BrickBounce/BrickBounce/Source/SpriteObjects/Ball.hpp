#ifndef BALL_H
#define BALL_H

#include "SpriteObject.hpp"
#include "Ball.hpp"

class Ball : public SpriteObject {
private:
	float speed;
	sf::Vector2f velocity;
	SpriteObject* attachedToObject;

	void bounce(SpriteObject* other, sf::FloatRect& intersection);
	void playBounceSound();
	sf::Vector3f solveCollision(const sf::FloatRect& intersection, const sf::Vector2f& collisionNormal);
public:
	Ball(const std::string& texturePath, sf::Vector2f position, float speed);
	void update(sf::RenderTarget& window, float elapsedTime) override;
	void draw(sf::RenderTarget& window, sf::RenderStates states) const override;
	void onCollision(SpriteObject* other, sf::FloatRect intersection) override;
	void setAttach(SpriteObject* other);
	void setVelocity(sf::Vector2f velocity);
};

#endif
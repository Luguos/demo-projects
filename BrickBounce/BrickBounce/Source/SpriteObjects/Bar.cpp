#include "Bar.hpp"

Bar::Bar(const std::string& texturePath, sf::Vector2f position, float speed, Ball* ball) : SpriteObject(position, texturePath), speed(speed), ball(ball) {
	ball->setAttach(this);
}

void Bar::update(sf::RenderTarget& window, float elapsedTime) {
	SpriteObject::update(window, elapsedTime);
	sf::Vector2f newPosition = getPosition();

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
		newPosition.x -= speed * elapsedTime;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
		newPosition.x += speed * elapsedTime;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
		ball->setAttach(NULL);
		ball->setVelocity(sf::Vector2f(-1, -1));
	}

	if (newPosition.x < 0) {
		newPosition.x = 0;
	}

	float windowEdge = window.getSize().x - (sprite.getTexture()->getSize().x * sprite.getScale().x);
	if (newPosition.x > windowEdge) {
		newPosition.x = windowEdge;
	}

	setPosition(newPosition);
}

void Bar::draw(sf::RenderTarget& window, sf::RenderStates states) const
{
	SpriteObject::draw(window, states);
}

void Bar::onCollision(SpriteObject* other, sf::FloatRect intersection) {
}
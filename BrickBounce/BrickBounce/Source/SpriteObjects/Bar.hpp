#ifndef BAR_H
#define BAR_H

#include "SpriteObject.hpp"
#include "Ball.hpp"

class Bar : public SpriteObject {
private:
	float speed;
	Ball* ball;
public:
	Bar(const std::string& texturePath, sf::Vector2f position, float speed, Ball* ball);
	void update(sf::RenderTarget& window, float elapsedTime) override;
	void draw(sf::RenderTarget& window, sf::RenderStates states) const override;
	void onCollision(SpriteObject* other, sf::FloatRect intersection) override;
};

#endif
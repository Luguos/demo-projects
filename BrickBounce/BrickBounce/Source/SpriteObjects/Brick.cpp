#include "Source/GameManager.hpp"
#include "Brick.hpp"

Brick::Brick(const std::string& texturePath, sf::Vector2f position, sf::Color color, int health, int points) : SpriteObject(position, texturePath) {
	this->health = health;
	this->points = points;
	sprite.setColor(color);
}

void Brick::update(sf::RenderTarget& window, float elapsedTime) {
	SpriteObject::update(window, elapsedTime);
}

void Brick::draw(sf::RenderTarget& window, sf::RenderStates states) const
{
	SpriteObject::draw(window, states);
}

void Brick::onCollision(SpriteObject* other, sf::FloatRect intersection) {
}

void Brick::hit() {
	health -= 1;
	if (health <= 0) {
		setActive(false);
		GameManager::addPoints(points);
	}
}
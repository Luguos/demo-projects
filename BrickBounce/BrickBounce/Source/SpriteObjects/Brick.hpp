#ifndef BRICK_H
#define BRICK_H

#include "SpriteObject.hpp"

class Brick : public SpriteObject {
private:
	int health;
	int points;
public:
	Brick(const std::string& texturePath, sf::Vector2f position, sf::Color color, int health, int points);
	void update(sf::RenderTarget& window, float elapsedTime) override;
	void draw(sf::RenderTarget& window, sf::RenderStates states) const override;
	void onCollision(SpriteObject* other, sf::FloatRect intersection) override;
	void hit();
};

#endif
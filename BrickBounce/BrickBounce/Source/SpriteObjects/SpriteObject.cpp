#include "SpriteObject.hpp"

SpriteObject::SpriteObject(sf::Vector2f position, const std::string& texturePath): GameObject(position) {
	if (!texture.loadFromFile(texturePath)) {
		// error when loading file
	}

	sprite.setTexture(texture);
}

void SpriteObject::update(sf::RenderTarget& window, float elapsedTime) {
}

void SpriteObject::draw(sf::RenderTarget& window, sf::RenderStates states) const {
	states.transform *= getTransform();
	window.draw(sprite, states);
}

sf::Sprite SpriteObject::getSprite() {
	return sprite;
}

sf::FloatRect SpriteObject::getLocalBounds() {
	return sprite.getLocalBounds();
}
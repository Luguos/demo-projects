#ifndef SPRITEOBJECT_H
#define SPRITEOBJECT_H

#include <string>
#include <SFML/Graphics.hpp>
#include "Source/GameObject.hpp"

class SpriteObject : public GameObject {
private:
	sf::Texture texture;
protected:
	sf::Sprite sprite;
public:
	SpriteObject(sf::Vector2f position, const std::string& texturePath);
	virtual void update(sf::RenderTarget& window, float elapsedTime) override;
	virtual void draw(sf::RenderTarget& window, sf::RenderStates states) const;
	virtual void onCollision(SpriteObject* other, sf::FloatRect intersection) = 0;
	sf::Sprite getSprite();
	sf::FloatRect getLocalBounds();
};

#endif
#include "TextManager.hpp"

sf::Font TextManager::font;
std::vector<sf::Text*> TextManager::texts;

void TextManager::loadFont(const std::string& fontPath) {
	if (!font.loadFromFile(fontPath)) {
		// error when loading file
	}
}

sf::Text* TextManager::createText(const std::string& text, int fontSize, sf::Color fontColor, sf::Vector2f position) {
	sf::Text* textObject = new sf::Text();
	textObject->setString(text);
	textObject->setFont(font);
	textObject->setCharacterSize(fontSize);
	textObject->setFillColor(fontColor);
	textObject->setPosition(position);
	texts.push_back(textObject);
	return textObject;
}

void TextManager::draw(sf::RenderTarget& window) {
	std::vector<sf::Text*>::iterator iterator;
	for (iterator = texts.begin(); iterator != texts.end(); iterator++) {
		window.draw(**iterator);
	}
}

void TextManager::clean() {
	std::vector<sf::Text*>::iterator iterator;
	for (iterator = texts.begin(); iterator != texts.end(); iterator++) {
		delete *iterator;
	}

	texts.clear();
}
#ifndef TEXTMANAGER_H
#define TEXTMANAGER_H

#include <string>
#include <vector>
#include <SFML/Graphics.hpp>

class TextManager {
private:
	static sf::Font font;
	static std::vector<sf::Text*> texts;
public:
	static void loadFont(const std::string& font);
	static sf::Text* createText(const std::string& text, int fontSize, sf::Color fontColor, sf::Vector2f position);
	static void draw(sf::RenderTarget& window);
	static void clean();
};

#endif
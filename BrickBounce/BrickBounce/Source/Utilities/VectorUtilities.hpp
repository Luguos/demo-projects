#ifndef VECTORUTILITIES_H
#define VECTORUTILITIES_H

#include <SFML/System.hpp>

float dot(const sf::Vector2f& left, const sf::Vector2f& right) {
	return left.x * right.x + left.y * right.y;
}

sf::Vector2f normalize(sf::Vector2f vector) {
	float dotProduct = dot(vector, vector);
	float length = std::sqrt(dotProduct);
	if (length != 0) {
		vector /= length;
	}
	return vector;
}

sf::Vector2f reflect(const sf::Vector2f& vector, const sf::Vector2f& normal) {
	return -2.0f * dot(vector, normal) * normal + vector;
}

#endif
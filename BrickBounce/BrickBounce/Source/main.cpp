#include <string>
#include <vector>
#include <SFML/Graphics.hpp>
#include "GameManager.hpp"

int main()
{
	sf::RenderWindow window(sf::VideoMode(1024, 768), "Brick Bounce");
	sf::Clock clock;
	GameManager manager;

	std::vector<GameObject*> gameObjects;
	manager.create(gameObjects, window);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) {
				window.close();
			}
		}

		window.clear();
		manager.update(gameObjects, window, clock);
		window.display();
	}

	manager.clean(gameObjects);

	return 0;
}
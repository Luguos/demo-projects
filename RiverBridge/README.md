# River Bridge

## Table of Contents
* [Game Summary](#game-summary)
* [Instructions](#instructions)
    * [Server](#server)
    * [Client](#client)

## Game Summary
In River Bridge, the goal of the game is to build a bridge to the other side of the river as fast as possible. This is done by pressing the buttons of the four different components for the bridge in the correct order. With every piece of the bridge that is built, the components shuffle in a random order.

The order in which the components have to be built is:
1. Pillars
2. Planks
3. Nails
4. Handles

## Instructions

### Server
The server is built using the Spring framework using the Java programming language. The data is saved in an in-memory H2 database, which will reset everytime the server is restarted. Communication with the server goes via a REST API.
Build and run the project from [river-bridge-server-java](https://gitlab.com/Luguos/demo-projects/-/tree/main/RiverBridge/river-bridge-server-java). *Note: Java JDK required.*

### Client
Build and run the project from [river-bridge-client-unity](https://gitlab.com/Luguos/demo-projects/-/tree/main/RiverBridge/river-bridge-client-unity).
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class BridgeBuilder : MonoBehaviour
{
    public static BridgeBuilder _instance;

    public Player player;
    public GameObject bridge;
    public GameObject[] buttons;

    [HideInInspector]
    public int currentPiece;
    public float tumbleTimeSeconds;

    [HideInInspector]
    public float time;
    public Text timeText;

    [HideInInspector]
    public List<int> inputOrder;

    public GameObject finishPopup;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        currentPiece = 0;
        time = 0;

        Random.InitState(Environment.TickCount);
        RandomizeButtons();
    }

    private void Update()
    {
        UpdateTime();

        // If all components are built, disable the input and move the player to the next piece.
        if (inputOrder.Count == Enum.GetValues(typeof(ComponentType)).Length && !player.isMoving)
        {
            SetButtonsActive(false);
            player.SetColor(player.correctColor);
            player.SetTargetLocation(new Vector3(1f, 0f, 0f));
            player.isMoving = true;
            StartCoroutine(player.Move());
        }
    }

    public BridgePiece GetCurrentPiece()
    {
        return bridge.transform.GetChild(currentPiece).GetComponent<BridgePiece>();

    }

    public void StartTumble()
    {
        SetButtonsActive(false);
        player.SetColor(player.incorrectColor);
        StartCoroutine(FinishTumble());
    }

    private IEnumerator FinishTumble()
    {
        // Wait for a few seconds before the player can continue.
        yield return new WaitForSecondsRealtime(tumbleTimeSeconds);
        player.SetColor(player.standardColor);
        SetButtonsActive(true);
    }

    public void FinishMove()
    {
        // Finish moving to the next bridge piece. If the entire bridge is finished, enable the Finish Popup.
        player.SetColor(player.standardColor);
        currentPiece++;
        inputOrder.Clear();

        if (currentPiece > bridge.transform.childCount - 1)
        {
            finishPopup.SetActive(true);
            finishPopup.GetComponent<FinishPopup>().Init(timeText.text);
        }
        else
        {
            RandomizeButtons();
            SetButtonsActive(true);
        }
    }

    public void SetButtonsActive(bool buttonsActive)
    {
        foreach (GameObject button in buttons)
        {
            BridgeButton bridgeButton = button.GetComponent<BridgeButton>();
            bridgeButton.enabled = buttonsActive;
        }
    }

    public void RandomizeButtons()
    {
        // Shuffle the array of bridge components using Fisher-Yates algorithm and assign them to the buttons.
        ComponentType[] componentArray = (ComponentType[])Enum.GetValues(typeof(ComponentType));

        int arrayCount = componentArray.Length;
        while (arrayCount > 1)
        {
            arrayCount--;
            int randomindex = Random.Range(0, 4);
            ComponentType tempValue = componentArray[randomindex];
            componentArray[randomindex] = componentArray[arrayCount];
            componentArray[arrayCount] = tempValue;

        }

        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].GetComponent<BridgeButton>().SetBridgeComponent(componentArray[i]);
        }
    }

    public void UpdateTime()
    {
        time += Time.deltaTime;

        int hours = Mathf.FloorToInt(time / 3600f);
        int minutes = Mathf.FloorToInt((time % 3600f) / 60f);
        int seconds = Mathf.FloorToInt(time % 60f);
        int milliseconds = Mathf.FloorToInt((time * 1000f) % 100f);

        timeText.text = string.Format("{0:00}:{1:00}:{2:00}:{3:00}", hours, minutes, seconds, milliseconds);
    }
}
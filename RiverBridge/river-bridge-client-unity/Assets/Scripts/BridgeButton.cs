using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class BridgeButton : MonoBehaviour
{
    public ComponentType componentType;
    public Image pieceImage;
    public InputAction buttonAction;

    public Sprite postsSprite;
    public Sprite planksSprite;
    public Sprite nailsSprite;
    public Sprite handlesSprite;

    private void OnEnable()
    {
        buttonAction.Enable();
    }

    private void OnDisable()
    {
        buttonAction.Disable();
    }

    private void Start()
    {
        buttonAction.performed += ctx => { ButtonPressed(); };
    }

    public void SetBridgeComponent(ComponentType newComponent)
    {
        // Change the button sprite based on the component type.
        componentType = newComponent;

        switch (newComponent)
        {
            case ComponentType.POSTS:
                pieceImage.sprite = postsSprite;
                break;
            case ComponentType.PLANKS:
                pieceImage.sprite = planksSprite;
                break;
            case ComponentType.NAILS:
                pieceImage.sprite = nailsSprite;
                break;
            case ComponentType.HANDLES:
                pieceImage.sprite = handlesSprite;
                break;
            default:
                pieceImage.sprite = null;
                break;
        }
    }

    public void ButtonPressed()
    {
        GameObject bridgeComponentObject = null;

        switch (componentType)
        {
            case ComponentType.POSTS:
                bridgeComponentObject = BridgeBuilder._instance.GetCurrentPiece().postsObject;
                break;
            case ComponentType.PLANKS:
                bridgeComponentObject = BridgeBuilder._instance.GetCurrentPiece().planksObject;
                break;
            case ComponentType.NAILS:
                bridgeComponentObject = BridgeBuilder._instance.GetCurrentPiece().nailsObject;
                break;
            case ComponentType.HANDLES:
                bridgeComponentObject = BridgeBuilder._instance.GetCurrentPiece().handlesObject;
                break;
            default:
                bridgeComponentObject = null;
                break;
        }

        // If the size of inputOrder is equal to the number of the component, it means the correct component button was pressed. Requires the enum of ComponentType to be in the correct order of building a piece.
        // When the incorrect button is pressed, all bridge pieces built so far are disabled and the player tumbles over.

        if (BridgeBuilder._instance.inputOrder.Count == (int)componentType)
        {
            bridgeComponentObject.SetActive(true);
            BridgeBuilder._instance.inputOrder.Add((int)componentType);
        }
        else
        {
            foreach (Transform bridgeComponent in BridgeBuilder._instance.GetCurrentPiece().transform)
            {
                bridgeComponent.gameObject.SetActive(false);
                BridgeBuilder._instance.inputOrder.Clear();
            }

            BridgeBuilder._instance.StartTumble();
        }
    }
}

// ComponentTypes should be in order of how they should be built, 0 being the first piece to build.
public enum ComponentType
{
    POSTS = 0,
    PLANKS = 1,
    NAILS = 2,
    HANDLES = 3
}
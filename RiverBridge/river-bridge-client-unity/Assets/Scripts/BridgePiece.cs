using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgePiece : MonoBehaviour
{
    // Keep track of which child object belongs to which bridge component.
    public GameObject postsObject;
    public GameObject planksObject;
    public GameObject nailsObject;
    public GameObject handlesObject;
}

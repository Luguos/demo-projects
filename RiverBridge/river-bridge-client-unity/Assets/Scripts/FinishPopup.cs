using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.Networking;
using System.Globalization;

public class FinishPopup : MonoBehaviour
{
    public Text contentText;
    public string menuScene;

    private void OnEnable()
    {
        Time.timeScale = 0f;

    }

    private void OnDisable()
    {
        Time.timeScale = 1f;
    }

    public void Init(string finalTime)
    {
        contentText.text = string.Format("You have completed the level with a final time of {0}!", finalTime);

        // Parse the final time to compare it with the parsed highscore and set a new highscore if necessary.
        if (DateTime.ParseExact(finalTime, "hh:mm:ss:ff", CultureInfo.InvariantCulture) > DateTime.ParseExact(SessionDetails._highscore, "hh:mm:ss:ff", CultureInfo.InvariantCulture))
        {
            SessionDetails._highscore = finalTime;
            SendHighscore();
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Exit()
    {
        SceneManager.LoadScene(menuScene);
    }

    public void SendHighscore()
    {
        // Send the new highscore to the server in JSON format.
        Highscore newHighscore = new Highscore(SessionDetails._id, SessionDetails._highscore);
        UnityWebRequest request = UnityWebRequest.Put(
            RestClient._instance.GenerateUrl(string.Format("/highscores/{0}", newHighscore.id)),
            JsonUtility.ToJson(newHighscore)
        );
        request.SetRequestHeader("content-Type", "application/json");
        StartCoroutine(RestClient._instance.SendRequest(request, (highscoreResult) => { }));
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Highscore
{
    public long id;
    public string highscore;

    public Highscore(long id, string highscore)
    {
        this.id = id;
        this.highscore = highscore;
    }

    public Highscore(string highscore)
    {
        this.highscore = highscore;
    }
}

using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Login : MonoBehaviour
{
    public InputField usernameInput;
    public InputField passwordInput;
    public MenuButtons menuButtons;

    public void ProcessLogin()
    {
        // Send a POST request to the server with the username and password. If they are correct, set them in the SessionDetails and
        // collect the highscore from the player's id. Then switch to the main menu.
        WWWForm form = new WWWForm();
        form.AddField("username", usernameInput.text);
        form.AddField("password", passwordInput.text);

        UnityWebRequest request = UnityWebRequest.Post(RestClient._instance.GenerateUrl("/users/login"), form);
        StartCoroutine(RestClient._instance.SendRequest(request, (userResult) =>
        {
            User resultUser = JsonUtility.FromJson<User>(userResult);
            SessionDetails._id = resultUser.id;
            SessionDetails._username = resultUser.username;
            UnityWebRequest request = UnityWebRequest.Get(RestClient._instance.GenerateUrl(string.Format("/highscores/{0}", SessionDetails._id)));
            StartCoroutine(RestClient._instance.SendRequest(request, (highscoreResult) =>
            {
                Highscore resultHighscore = JsonUtility.FromJson<Highscore>(highscoreResult);
                SessionDetails._highscore = resultHighscore.highscore;
                menuButtons.ChangeWindow(1);
            }));
        }));
    }

    public void ProcessSignup()
    {
        // Send a PUT request to the server with the information of the new user. If the username is not taken, set them in the SessionDetails and
        // create the highscore for the player's id. Then switch to the main menu.
        User newUser = new User(usernameInput.text, passwordInput.text);
        UnityWebRequest request = UnityWebRequest.Put(RestClient._instance.GenerateUrl("/users/signup"), JsonUtility.ToJson(newUser));
        request.SetRequestHeader("content-Type", "application/json");
        StartCoroutine(RestClient._instance.SendRequest(request, (userResult) =>
        {
            User resultUser = JsonUtility.FromJson<User>(userResult);
            SessionDetails._id = resultUser.id;
            SessionDetails._username = resultUser.username;

            Highscore newHighscore = new Highscore("00:00:00:00");
            UnityWebRequest request = UnityWebRequest.Put(
                RestClient._instance.GenerateUrl(string.Format("/highscores/{0}", SessionDetails._id)),
                JsonUtility.ToJson(newHighscore)
            );
            request.SetRequestHeader("content-Type", "application/json");
            StartCoroutine(RestClient._instance.SendRequest(request, (highscoreResult) =>
            {
                Highscore resultHighscore = JsonUtility.FromJson<Highscore>(highscoreResult);
                SessionDetails._highscore = resultHighscore.highscore;
                menuButtons.ChangeWindow(1);
            }));
        }));
    }
}

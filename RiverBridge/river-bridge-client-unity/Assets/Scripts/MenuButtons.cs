using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButtons : MonoBehaviour
{
    public GameObject[] windows;
    public int startingWindow;
    private int currentWindow;

    public string playScene;

    void Start()
    {
        // Disable all windows except the starting window in case they weren't in the editor.
        foreach (GameObject window in windows)
        {
            window.SetActive(false);
        }

        windows[startingWindow].SetActive(true);
        currentWindow = startingWindow;
    }

    public void ChangeWindow(int newWindow)
    {
        windows[currentWindow].SetActive(false);
        windows[newWindow].SetActive(true);
        currentWindow = newWindow;
    }

    public void PlayButton()
    {
        SceneManager.LoadScene(playScene);
    }

    public void QuitButton()
    {
        Application.Quit();
    }
}

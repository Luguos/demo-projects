using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public SpriteRenderer playerRenderer;
    public Color standardColor;
    public Color correctColor;
    public Color incorrectColor;

    public bool isMoving;

    private Vector3 targetLocation;

    private void Start()
    {
        isMoving = false;
        SetColor(standardColor);
    }

    public void SetTargetLocation(Vector3 moveOffset)
    {
        targetLocation = transform.position + moveOffset;
    }

    public IEnumerator Move()
    {
        // Move to the target location and fire the FinishMove() callback when finished.
        while (transform.position != targetLocation)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetLocation, Time.deltaTime);
            yield return null;
        }

        isMoving = false;
        BridgeBuilder._instance.FinishMove();
        yield return null;
    }

    public void SetColor(Color color)
    {
        playerRenderer.color = color;
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class RestClient : MonoBehaviour
{
    public static RestClient _instance;
    private string baseUrl = "http://localhost:8080";
    public Canvas sessionCanvas;

    private void Awake()
    {
        if (_instance == null)
        {
            DontDestroyOnLoad(gameObject);
            _instance = this;
        }
        else
        {
            Destroy(this);
        }

        sessionCanvas.gameObject.SetActive(false);
    }

    public IEnumerator SendRequest(UnityWebRequest request, Action<string> onComplete)
    {
        // Make a request, showing a loading window while processing. Execute the onComplete action when finished succesfully.
        sessionCanvas.gameObject.SetActive(true);

        yield return request.SendWebRequest();

        if (request.result == UnityWebRequest.Result.Success)
        {
            string jsonResult = request.downloadHandler.text;
            onComplete(jsonResult);
        }
        else
        {
            Debug.Log(request.error);
        }

        sessionCanvas.gameObject.SetActive(false);
    }

    public string GenerateUrl(string endpoint)
    {
        return baseUrl + endpoint;
    }
}

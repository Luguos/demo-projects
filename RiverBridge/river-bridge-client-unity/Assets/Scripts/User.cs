using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class User
{
    public long id;
    public string username;
    public string password;

    public User(long id, string username, string password)
    {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public User(string username, string password)
    {
        this.username = username;
        this.password = password;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterScrolling : MonoBehaviour
{
    public float speed;
    public MeshRenderer _renderer;

    void Update()
    {
        // Adjust the offset of the texture's UVs to create a scrolling effect.
        float offsetValue = Time.time * speed;
        _renderer.material.SetTextureOffset("_MainTex", new Vector2(offsetValue, offsetValue)); 
    }
}

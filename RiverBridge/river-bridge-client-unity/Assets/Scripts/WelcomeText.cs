using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WelcomeText : MonoBehaviour
{
    public Text welcomeText;

    private void OnEnable()
    {
        SetText(SessionDetails._username, SessionDetails._highscore);
    }

    public void SetText(string username, string highscore)
    {
        welcomeText.text = string.Format("Welcome {0}!\nRecord time: {1}", username, highscore);
    }
}

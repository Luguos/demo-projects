package com.brianduncker.river.bridge.server.java;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Highscore {
    private @Id long id;
    private String highscore;
    
    public Highscore() {}
    
    public Highscore(long id, String highscore) {
        this.id = id;
        this.highscore = highscore;
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public String getHighscore() {
        return highscore;
    }

    public void setHighscore(String highscore) {
        this.highscore = highscore;
    }
}

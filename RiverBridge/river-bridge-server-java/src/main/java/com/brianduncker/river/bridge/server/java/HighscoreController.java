package com.brianduncker.river.bridge.server.java;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HighscoreController {

    private final HighscoreRepository repository;

    HighscoreController(HighscoreRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/highscores/{id}")
    public Highscore getHighscore(@PathVariable Long id) {
        return repository.findById(id).orElseThrow(() -> new RuntimeException("Highscore not found!"));
    }

    @PostMapping("/highscores")
    public Highscore addHighscore(@RequestBody Highscore newHighscore) {
        return repository.save(newHighscore);
    }

    @PutMapping("/highscores/{id}")
    public Highscore updateHighscore(@RequestBody Highscore newHighscore, @PathVariable Long id) {
        return repository.findById(id)
                .map(oldHighscore -> {
                    oldHighscore.setHighscore(newHighscore.getHighscore());
                    return repository.save(oldHighscore);
                })
                .orElseGet(() -> {
                    newHighscore.setId(id);
                    return repository.save(newHighscore);
                });
    }
}

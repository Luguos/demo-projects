package com.brianduncker.river.bridge.server.java;

import org.springframework.data.jpa.repository.JpaRepository;

public interface HighscoreRepository extends JpaRepository<Highscore, Long> {
    
}

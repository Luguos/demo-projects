package com.brianduncker.river.bridge.server.java;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoadTestData {
    
    @Autowired
    PasswordEncoder passwordEncoder;
    
    private static final Logger logger = LoggerFactory.getLogger(LoadTestData.class);
    
    private final User user1 = new User("TestUser1", "test123");
    private final User user2 = new User("TestUser2", "test456");
    
    // Load the database with some sample data for testing purposes.
    
    @Bean
    CommandLineRunner initUserData(UserRepository repository) {
        return args -> {
            user1.setPassword(passwordEncoder.encodePassword(user1.getPassword()));
            user2.setPassword(passwordEncoder.encodePassword(user2.getPassword()));
            logger.info("Loading User: " + repository.save(user1));
            logger.info("Loading User: " + repository.save(user2));
        };
    }
    
    @Bean
    CommandLineRunner initHighscoreData(HighscoreRepository repository) {
        return args -> {
            logger.info("Loading Highscore: " + repository.save(new Highscore(user1.getId(), "00:00:00:00")));
            logger.info("Loading Highscore: " + repository.save(new Highscore(user2.getId(), "00:00:00:00")));
        };
    }
}

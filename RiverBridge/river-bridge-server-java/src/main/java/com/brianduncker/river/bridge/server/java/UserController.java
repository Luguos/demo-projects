package com.brianduncker.river.bridge.server.java;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    private final UserRepository repository;

    @Autowired
    PasswordEncoder passwordEncoder;
    
    UserController(UserRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/users/{id}")
    public User getUser(@PathVariable Long id) {
        return repository.findById(id).orElseThrow(() -> new RuntimeException("User not found!"));
    }

    // Add a new user, check if the username already exists beforehand.
    @PutMapping("/users/signup")
    public ResponseEntity addUser(@RequestBody User newUser) {
        if (repository.findByUsername(newUser.getUsername()).isEmpty()) {
            newUser.setPassword(passwordEncoder.encodePassword(newUser.getPassword()));
            return ResponseEntity.status(HttpStatus.OK).body(repository.save(newUser));
        }
        else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Username already exists!");
        }
    }

    // Find the user by username, and verify the password.
    @PostMapping("/users/login")
    public ResponseEntity loginUser(@RequestParam String username, @RequestParam String password) {
        User foundUser = repository.findByUsername(username).orElseThrow(() -> new RuntimeException("User not found!"));
        if (passwordEncoder.comparePassword(password, foundUser.getPassword())) {
            return ResponseEntity.status(HttpStatus.OK).body(foundUser);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found!");
        }
    }

    @PutMapping("/users/{id}")
    public User updateUser(@RequestBody User newUser, @PathVariable Long id) {
        return repository.findById(id)
                .map(oldUser -> {
                    oldUser.setUsername(newUser.getUsername());
                    oldUser.setPassword(passwordEncoder.encodePassword(newUser.getPassword()));
                    return repository.save(oldUser);
                })
                .orElseGet(() -> {
                    newUser.setId(id);
                    return repository.save(newUser);
                });
    }
}
